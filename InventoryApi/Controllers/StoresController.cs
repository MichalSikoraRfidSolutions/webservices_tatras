﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {
        private readonly IDatabaseManager database;
        public StoresController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }

        // GET: api/Stores
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var stores = database.GetStores();
                
                return Ok(stores);
            }
            catch (Exception ex)
            {
                

                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString()+ Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);  }
        }
       

     
    }
}
