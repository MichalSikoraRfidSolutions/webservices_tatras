using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class Tag
    {

        protected string FormatEPC(string epc)
        {
             

            StringBuilder sb = new StringBuilder();
        int m = 0;
            for (int n = 0; n<epc.Length; n++)
            {
                if (m == 4)
                {
                    sb.Append(" ");
                    m = 0;
                }
    m++;
                sb.Append(epc[n]);
            }
            return sb.ToString();
        }


        #region ID

        protected int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Status

        protected TagStatus m_Status;

        public TagStatus Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }

        public string StatusName
        {
            get
            {
                switch (m_Status)
                {
                    case TagStatus.Invalid:
                        return "Niewa�ny";
                    case TagStatus.New:
                        return "Nowy";
                    case TagStatus.Registered:
                        return "Zarejestrowany";
                    case TagStatus.In:
                        return "Na Stanie";
                    case TagStatus.Out:
                        return "Wydany";
                    default:
                        return "";
                }
            }
        }

        public string StatusCode
        {
            set
            {
                switch (value)
                {
                    case "E":
                        m_Status = TagStatus.Invalid;
                        break;
                    case "N":
                        m_Status = TagStatus.New;
                        break;
                    case "R":
                        m_Status = TagStatus.Registered;
                        break;
                    case "I":
                        m_Status = TagStatus.In;
                        break;
                    case "O":
                        m_Status = TagStatus.Out;
                        break;
                    default:
                        m_Status = TagStatus.Invalid;
                        break;
                }
            }
            get
            {
                switch (m_Status)
                {
                    case TagStatus.Invalid:
                        return "E";
                    case TagStatus.New:
                        return "N";
                    case TagStatus.Registered:
                        return "R";
                    case TagStatus.In:
                        return "I";
                    case TagStatus.Out:
                        return "O";
                    default:
                        return "E";
                }
            }
        }
        #endregion

        #region ProductID

        protected int m_ProductID;

        public int ProductID
        {
            get { return m_ProductID; }
            set { m_ProductID = value; }
        }

        #endregion

        #region ProductName

        protected string m_ProductName;

        public string ProductName
        {
            get { return m_ProductName; }
            set { m_ProductName = value; }
        }

        #endregion

        #region ProductCode

        protected string m_ProductCode;

        public string ProductCode
        {
            get { return m_ProductCode; }
            set { m_ProductCode = value; }
        }

        #endregion

        #region Code

        protected string m_Code;

        public string Code
        {
            get { if (m_Code != null) return m_Code.Replace(" ", ""); else return m_Code; }
            set { m_Code = FormatEPC(value); }
        }

        #endregion

        #region CustomerID

        protected int m_CustomerID;

        public int CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }

        #endregion

        #region CustomerName

        protected string m_CustomerName;

        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }

        #endregion

        #region StoreID

        protected int m_StoreID;

        public int StoreID
        {
            get { return m_StoreID; }
            set { m_StoreID = value; }
        }

        #endregion

        #region StoreName

        protected string m_StoreName;

        public string StoreName
        {
            get { return m_StoreName; }
            set { m_StoreName = value; }
        }

        #endregion
    }

    public enum TagStatus
    {
        New,
        Registered,
        In,
        Out,
        Invalid
    }

    public class TagList : List<Tag>
    {
    }

    public class OutputTags {
    
        public List<Tag> tags { set; get; }
        public int user_id { set; get; }
}
}
