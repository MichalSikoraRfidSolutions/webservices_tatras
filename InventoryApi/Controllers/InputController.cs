﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDObjects.MetaModel;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InputController : ControllerBase
    {
        private readonly IDatabaseManager database;
        public InputController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }

        // GET: api/Input
        [HttpPost] 
        public IActionResult Post([FromBody] WSInput wSInput)
        {
            try
            {
                database.SaveTagInput(wSInput.Operation, wSInput.Tags);
                return Ok("ok");
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
        }

    }
}
