using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class StockTaking
    {
        #region Properties

        #region ID

        private int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region UserID

        private int m_UserID;

        public int UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }

        #endregion

        #region SnapshotDate

        private DateTime m_SnapshotDate;

        public DateTime SnapshotDate
        {
            get { return m_SnapshotDate; }
            set { m_SnapshotDate = value; }
        }

        #endregion

        #region StockTakingDate

        private DateTime m_StockTakingDate;

        public DateTime StockTakingDate
        {
            get { return m_StockTakingDate; }
            set { m_StockTakingDate = value; }
        }

        #endregion

        #region Notes

        private string m_Notes;

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        #endregion

        #region StoreID

        private int m_StoreID;

        public int StoreID
        {
            get { return m_StoreID; }
            set { m_StoreID = value; }
        }

        #endregion

        #region StoreName

        private string m_StoreName;

        public string StoreName
        {
            get { return m_StoreName; }
            set { m_StoreName = value; }
        }

        #endregion

        #region ProductGroupID

        private int m_ProductGroupID;

        public int ProductGroupID
        {
            get { return m_ProductGroupID; }
            set { m_ProductGroupID = value; }
        }

        #endregion

        #region ProductGroupName

        private string m_ProductGroupName;

        public string ProductGroupName
        {
            get { return m_ProductGroupName; }
            set { m_ProductGroupName = value; }
        }

        #endregion

        #region SaveDate

        private DateTime m_SaveDate;

        public DateTime SaveDate
        {
            get { return m_SaveDate; }
            set { m_SaveDate = value; }
        }

        #endregion

        #region Products

        private StockTakingProductList m_Products;

        public StockTakingProductList Products
        {
            get { return m_Products; }
            set { m_Products = value; }
        }

        #endregion

        #region Status

        private StockTakingStatus m_Status;

        public StockTakingStatus Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }

        public string StatusName
        {
            get
            {
                if (m_Status == StockTakingStatus.New)
                    return "Nowa";
                else
                    return "Rozliczona";
            }
        }

        public string StatusCode
        {
            get
            {
                if (m_Status == StockTakingStatus.New)
                    return "N";
                else
                    return "C";
            }
            set
            {
                if (value == "N")
                    m_Status = StockTakingStatus.New;
                else
                    m_Status = StockTakingStatus.Consistent;
            }
        }
        #endregion

        #endregion
    }

    public enum StockTakingStatus
    {
        New,
        Consistent
    }

    public class StockTakingList : List<StockTaking>
    {
    }
}
