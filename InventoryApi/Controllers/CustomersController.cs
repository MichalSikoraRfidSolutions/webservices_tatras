﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {

        private readonly IDatabaseManager database;
        public CustomersController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }
        // GET: api/Customers
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var customers = database.GetCustomers(null);
                return Ok(customers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return BadRequest("Get Customers ERROR " + ex.Message);
            }
        }


        [HttpGet ("GetOverTypeID")]
        public IActionResult Get(int typeID)
        {
            if (typeID < 0)
                return BadRequest("Incorrect TypeID");
            
            try
            {
                var customers = database.GetCustomers(null, typeID);
                return Ok(customers);
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest("Get Customers ERROR " + ex.Message);
            }
        }

    }
}
