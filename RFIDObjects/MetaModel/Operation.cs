using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class Operation
    {
        #region ID

        private int m_ID;
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion


        #region Date

        private DateTime m_Date;
        public DateTime Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }

        #endregion

        #region User

       // private User m_User;
       // public User User
       //     get { return m_User; }
        //    set { m_User = value; }
       // }
        private int m_UserID;
        public int UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }

        #endregion

        #region Type

        private OperationType m_Type;
        public OperationType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        #endregion

        #region TypeCode

        public string TypeCode
        {
            get
            {
                switch (m_Type)
                {
                    case OperationType.Registration:
                        return "R";
                    case OperationType.Input:
                        return "I";
                    case OperationType.Output:
                        return "O";
                }
                return "U";
            }
            set
            {
                switch (value)
                {
                    case "I":
                        m_Type = OperationType.Input;
                        break;
                    case "O":
                        m_Type = OperationType.Output;
                        break;
                    case "R":
                        m_Type = OperationType.Registration;
                        break;
                    default:
                        m_Type = OperationType.Unknown;
                        break;
                }
            }
        }

        #endregion

        #region InStoreID

        private int m_InStoreID;
        public int InStoreID
        {
            get { return m_InStoreID; }
            set { m_InStoreID = value; }
        }

        #endregion

        #region OutStoreID

        private int m_OutStoreID;
        public int OutStoreID
        {
            get { return m_OutStoreID; }
            set { m_OutStoreID = value; }
        }

        #endregion

        #region CustomerID

        private int m_CustomerID;
        public int CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }

        #endregion
    }

    public enum OperationType
    {
        Unknown,
        Registration,
        Input,
        Output
    }

    public class OperationItem
    {
        #region ID

        private int m_ID;
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region OperationID

        private int m_OperationID;
        public int OperationID
        {
            get { return m_OperationID; }
            set { m_OperationID = value; }
        }

        #endregion

        #region TagID

        private int m_TagID;
        public int TagID
        {
            get { return m_TagID; }
            set { m_TagID = value; }
        }

        #endregion

    }

}
