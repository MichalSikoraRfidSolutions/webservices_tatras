﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class WSInput
    {
            public Operation Operation { get; set; }

            public List<Tag> Tags { get; set; }

            public WSInput()
            {
                Tags = new List<Tag>();
            }
    }
}
