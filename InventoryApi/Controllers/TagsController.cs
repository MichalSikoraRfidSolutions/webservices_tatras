﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDObjects.MetaModel;
namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagsController : ControllerBase
    {
        
        protected string FormatEPC(string epc)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
            int m = 0;
            for (int n = 0; n < epc.Length; n++)
            {
                if (m == 4)
                {
                    sb.Append(" ");
                    m = 0;
                }
                m++;
                sb.Append(epc[n]);
            }
            return sb.ToString();
        }
        catch(Exception ex)
            {
            Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString()+ Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return epc;
            }
        }

        private readonly IDatabaseManager database;
        public TagsController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }
        // GET: api/Tags
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var tags = database.GetTags(0, 0);
                return Ok(tags);
            }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetByEpc")]
        public IActionResult Get(string Epc)
        {
            try
            {
                var tag = database.GetTag(FormatEPC(Epc));
                if (tag == null)
                {//fdfsdfsdfsdfsd
                 // RFIDObjects.MetaModel.Operation operation = new RFIDObjects.MetaModel.Operation)();
                 //  operation.Type = RFIDObjects.MetaModel.OperationType.Registration;
                 //  operation.User = new RFIDObjects.MetaModel.User();

                    //  database.SaveTagRegistration()
                    tag = new RFIDObjects.MetaModel.Tag();
                    tag.ID = -1;
                }
                return Ok(tag);
            }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
            }

        [HttpGet("TagByProductId")]
        public IActionResult TagByProductId(int ProductID)
        {
            try
            {
                var tag = database.TagByProductId(ProductID);
                if (tag == null)
                {
                    return BadRequest("tag value is null");
                }
                return Ok(tag);
            }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
            }

        [HttpPost("OutputTag")]
        public IActionResult OutpuTag([FromBody] OutputTags tags)
        {
            try
            {

                Operation operation = new Operation();
                operation.Type = OperationType.Output;
                operation.UserID = tags.user_id;
                operation.OutStoreID = tags.tags[0].StoreID; 
                operation.Date = DateTime.Now;
          
                database.SaveTagOutput(operation, tags.tags);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
        }
            
      

        [HttpPost("RegisterAndInputTag")]
        public IActionResult RegisterAndInputTag([FromBody] RegistrationAndInsert registrationAndInsert )
        {
            try
            {
                Operation operation = new  Operation();
                operation.Type = OperationType.Registration;
               // operation.Type = OperationType.Input;
                operation.UserID = registrationAndInsert.User_id;
                operation.InStoreID = registrationAndInsert.Store_id;
                operation.Date = DateTime.Now;
                database.SaveTagRegistration(operation, registrationAndInsert.TagList, registrationAndInsert.Product_id);
                return Ok("Ok");
            }
            catch (Exception ex)
            {
                database.RollbackTransaction();
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
        } 
    }
}
