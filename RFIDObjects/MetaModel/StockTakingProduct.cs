using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class StockTakingProduct
    {
        #region ProductID

        protected int m_ProductID;

        public int ProductID
        {
            get { return m_ProductID; }
            set { m_ProductID = value; }
        }

        #endregion

        #region ProductName

        protected string m_ProductName;

        public string ProductName
        {
            get { return m_ProductName; }
            set { m_ProductName = value; }
        }

        #endregion

        #region ProductCode

        protected string m_ProductCode;

        public string ProductCode
        {
            get { return m_ProductCode; }
            set { m_ProductCode = value; }
        }

        #endregion

        //#region ProductGroupID

        //protected int m_ProductGroupID;

        //public int ProductGroupID
        //{
        //    get { return m_ProductGroupID; }
        //    set { m_ProductGroupID = value; }
        //}

        //#endregion

        //#region ProductGroupName

        //protected string m_ProductGroupName;

        //public string ProductGroupName
        //{
        //    get { return m_ProductGroupName; }
        //    set { m_ProductGroupName = value; }
        //}

        //#endregion

        //#region StoreID

        //protected int m_StoreID;

        //public int StoreID
        //{
        //    get { return m_StoreID; }
        //    set { m_StoreID = value; }
        //}

        //#endregion

        //#region StoreName

        //protected string m_StoreName;

        //public string StoreName
        //{
        //    get { return m_StoreName; }
        //    set { m_StoreName = value; }
        //}

        //#endregion

        #region StockQty

        protected int m_StockQty;

        public int StockQty
        {
            get { return m_StockQty; }
            set { m_StockQty = value; }
        }

        #endregion

        #region CountedQty

        protected int m_CountedQty = 0;

        public int CountedQty
        {
            get { return m_CountedQty; }
            set { m_CountedQty = value; }
        }

        #endregion

        #region OutOfStoreQty

        protected int m_OutOfStoreQty = 0;

        public int OutOfStoreQty
        {
            get { return m_OutOfStoreQty; }
            set { m_OutOfStoreQty = value; }
        }

        #endregion

        #region OtherStoreQty

        protected int m_OtherStoreQty = 0;

        public int OtherStoreQty
        {
            get { return m_OtherStoreQty; }
            set { m_OtherStoreQty = value; }
        }

        #endregion

        #region RegisteredQty

        protected int m_RegisteredQty = 0;

        public int RegisteredQty
        {
            get { return m_RegisteredQty; }
            set { m_RegisteredQty = value; }
        }

        #endregion

        #region CorrectionQty

        protected int m_CorrectionQty = 0;

        public int CorrectionQty
        {
            get { return m_CorrectionQty; }
            set { m_CorrectionQty = value; }
        }

        #endregion

        #region Tags

        protected List<StockTakingTag> m_Tags = new List<StockTakingTag>();

        public List<StockTakingTag> Tags
        {
            get { return m_Tags; }
        }

        #endregion

        #region TagsLoaded

        protected bool m_TagsLoaded = false;

        public bool TagsLoaded
        {
            get { return m_TagsLoaded; }
            set { m_TagsLoaded = value; }
        }

        #endregion

    }

    public class StockTakingProductList : List<StockTakingProduct>
    {
    }
}
