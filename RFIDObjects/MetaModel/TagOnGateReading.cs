using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class TagOnGateReading
    {
        public TagOnGateReading(TagReading tag)
        {
            m_TagCode = tag.TagCode;
            m_FirstDetectionDate = tag.ReadDate;
            m_LastDetectionDate = tag.ReadDate;
        }

        private string m_TagCode;
        private DateTime m_FirstDetectionDate;
        private DateTime m_LastDetectionDate;

        public string TagCode
        {
            get { return m_TagCode; }
        }

        public DateTime FirstDetectionDate
        {
            get { return m_FirstDetectionDate; }
            set { m_FirstDetectionDate = value; }
        }

        public DateTime LastDetectionDate
        {
            get { return m_LastDetectionDate; }
            set { m_LastDetectionDate = value; }
        }
    }
}
