﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDObjects.MetaModel;
namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocktakingController : ControllerBase
    {
        private readonly IDatabaseManager database;

        public StocktakingController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }

        // GET: api/Stocktaking
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var s = database.GetStockTakings(DateTime.MinValue, DateTime.MaxValue, "N", 0);
                return Ok(s);
            }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message); }
        }

        [HttpGet("byId")]
        public IActionResult GetById(int id)
        {
            try
            {
                var s = database.GetStockTakings(DateTime.MinValue, DateTime.MaxValue, null, 0);

                foreach (StockTaking st in s)
                {
                    if (st.ID == id)
                    {
                        return Ok(st);

                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }

            return BadRequest();

        }

        [HttpPost]
        public IActionResult Save([FromBody] StockTaking stockTaking)
        {
            try
            {
                database.Save(stockTaking);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }

        }
    }
}
