using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApi.Services
{
 
    public class DatabaseManagerFactory
    {
        public static IDatabaseManager CreateDatabaseManager()
        {
            return new SqlDatabaseManager();
        }
    }
}
