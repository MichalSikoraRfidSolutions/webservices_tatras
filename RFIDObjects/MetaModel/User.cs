﻿namespace RFIDObjects.MetaModel
{
    public class User
    {
        #region Properties

        #region ID

        protected int m_ID;
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Login

        protected string m_Login;
        public string Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }

        #endregion

        #region Password

        protected string m_Password;
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        #endregion

        #region Surname

        protected string m_Surname;
        public string Surname
        {
            get { return m_Surname; }
            set { m_Surname = value; }
        }

        #endregion

        #region Forename

        protected string m_Forename;
        public string Forename
        {
            get { return m_Forename; }
            set { m_Forename = value; }
        }

        #endregion

        #region Active

        protected bool m_Active;
        public bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        #endregion

        #region Privileges

        protected int m_Privileges;
        public int Privileges
        {
            get { return m_Privileges; }
            set { m_Privileges = value; }
        }
        #endregion

        #endregion

    }
}
