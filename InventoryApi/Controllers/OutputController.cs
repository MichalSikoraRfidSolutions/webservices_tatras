﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDObjects.MetaModel;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OutputController : ControllerBase
    {
        private readonly IDatabaseManager database;
        public OutputController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }
        // POST: api/Output
        [HttpPost]
        public IActionResult Post([FromBody] WSOutput wSOutput)
        {
            try
            {
                database.SaveTagOutput(wSOutput.Operation, wSOutput.Tags);
                return Ok("ok");
                    }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }
        }

       
    }
}
