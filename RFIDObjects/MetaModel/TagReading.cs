using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class TagReading
    {
        public TagReading(string tagCode, int antennaID, DateTime date)
        {
            m_TagCode = tagCode;
            m_AntennaID = antennaID;
            m_ReadDate = date;
        }

        private string m_TagCode;
        private int m_AntennaID;
        private DateTime m_ReadDate;

        public string TagCode
        {
            get { return m_TagCode; }
        }

        public int AntennaID
        {
            get { return m_AntennaID; }
        }

        public DateTime ReadDate
        {
            get { return m_ReadDate; }
        }
    }
}
