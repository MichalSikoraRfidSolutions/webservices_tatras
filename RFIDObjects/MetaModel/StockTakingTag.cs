using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class StockTakingTag
    {
        public StockTakingTag(Tag tag)
        {
            m_Tag = tag;
        }

        #region Properties

        #region Tag

        protected Tag m_Tag;

        public Tag Tag
        {
            get { return m_Tag; }
            set { m_Tag = value; }
        }

        #endregion

        #region Code

        public string Code
        {
            get
            {
                if (m_Tag != null)
                    return m_Tag.Code;
                else
                    return null;
            }
        }

        #endregion

        #region StatusName

        public string StatusName
        {
            get
            {
                if (m_Tag != null)
                    return m_Tag.StatusName;
                else
                    return null;
            }
        }

        #endregion

        #region StoreName

        public string StoreName
        {
            get
            {
                if (m_Tag != null)
                    return m_Tag.StoreName;
                else
                    return null;
            }
        }

        #endregion

        #region Counted

        protected bool m_Counted = false;

        public bool Counted
        {
            get { return m_Counted; }
            set { m_Counted = value; }
        }

        #endregion

        #region ReadDate

        private DateTime m_ReadDate;

        public DateTime ReadDate
        {
            get { return m_ReadDate; }
            set { m_ReadDate = value; }
        }

        #endregion

        #region Correction

        private CorrectionType m_Correction;

        public CorrectionType Correction
        {
            get { return m_Correction; }
            set { m_Correction = value; }
        }

        public string CorrectionName
        {
            get
            {
                switch (m_Correction)
                {
                    case CorrectionType.Input:
                        return "Przyj�ty na stan";
                    case CorrectionType.Output:
                        return "Zdj�ty ze stanu";
                    default:
                        return "";
                }
            }
        }

        public string CorrectionCode
        {
            get
            {
                switch (m_Correction)
                {
                    case CorrectionType.Input:
                        return "I";
                    case CorrectionType.Output:
                        return "O";
                    default:
                        return "N";
                }
            }
            set
            {
                switch (value)
                {
                    case "I":
                        m_Correction = CorrectionType.Input;
                        break;
                    case "O":
                        m_Correction = CorrectionType.Output;
                        break;
                    default:
                        m_Correction = CorrectionType.None;
                        break;
                }
            }
        }

        #endregion


        #endregion
    }

    public class StockTakingTagList : List<StockTakingTag>
    {
    }

    public enum CorrectionType
    {
        None,
        Input,
        Output
    }
}
