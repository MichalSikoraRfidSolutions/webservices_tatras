﻿using System;
using System.Collections.Generic;
using System.Data;
using RFIDObjects.MetaModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace InventoryApi.Services
{

    public class SqlDatabaseManager : IDatabaseManager
    {
        private readonly IOptions<Configurations> m_config;
        public string connectionString;

        public SqlDatabaseManager(IOptions<Configurations> options)
        {
             m_config = options;
            //OpenConnection(config.Value.ConnectionString);
            connectionString = m_config.Value.ConnectionString;
        }

        public SqlDatabaseManager()
        {

        }



        #region Fields

        private SqlConnection m_Connection;
        private SqlTransaction m_Transaction;
        private SqlCommand m_Command;
        private int m_TransactionCount = 0;



        #endregion
        protected string FormatEPC(string epc)
        {


            StringBuilder sb = new StringBuilder();
            int m = 0;
            for (int n = 0; n < epc.Length; n++)
            {
                if (m == 4)
                {
                    sb.Append(" ");
                    m = 0;
                }
                m++;
                sb.Append(epc[n]);
            }
            return sb.ToString();
        }


        #region Database Methods

        public void OpenConnection(string connectionString)
        {
            try
            {
                m_Connection = new SqlConnection(connectionString);
                m_Connection.Open();
            }
            catch (Exception ex)
            {
                throw new DatabaseManagerException("Błąd otwarcia bazy danych.", ex);
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (m_Connection != null)
                    m_Connection.Close();
            }
            catch
            {
            }
        }

        private void CreateProcedureCommand(string name)
        {
            if (m_Transaction == null)
                m_Command = new SqlCommand(name, m_Connection);
            else
                m_Command = new SqlCommand(name, m_Connection, m_Transaction);
            m_Command.CommandType = CommandType.StoredProcedure;
        }

        private void AddParameter(string name, object value)
        {
            m_Command.Parameters.AddWithValue(name, value);
        }

        private void AddOutputParameter(string name)
        {
            SqlParameter param = new SqlParameter(name, SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            m_Command.Parameters.Add(param);
        }

        private void AddReturnParameter()
        {
            SqlParameter param = new SqlParameter("RET", SqlDbType.Int);
            param.Direction = ParameterDirection.ReturnValue;
            m_Command.Parameters.Add(param);
        }

        private int GetParameterValue(string name)
        {
            return Convert.ToInt32(m_Command.Parameters[name].Value);
        }

        private int GetReturnValue()
        {
            return Convert.ToInt32(m_Command.Parameters["RET"].Value);
        }

        private void ExecuteProcedure()
        {
            m_Command.ExecuteNonQuery();
        }

        private SqlDataReader ExecuteQuery()
        {
            return m_Command.ExecuteReader();
        }

        public void BeginTransaction()
        {
            m_TransactionCount++;
            if (m_Transaction == null)
                m_Transaction = m_Connection.BeginTransaction();
        }
        public void CommitTransaction()
        {
            m_TransactionCount--;
            if (m_Transaction != null && m_TransactionCount == 0)
            {
                m_Transaction.Commit();
                m_Transaction.Dispose();
                m_Transaction = null;
            }
        }

        public void RollbackTransaction()
        {
            m_TransactionCount--;
            if (m_Transaction != null && m_TransactionCount == 0)
            {
                m_Transaction.Rollback();
                m_Transaction.Dispose();
                m_Transaction = null;
            }
        }

        public void ExecuteProcedure(string procedureName)
        {
            CreateProcedureCommand(procedureName);
            ExecuteProcedure();
        }

        #endregion

        #region User

        public List<User> LoadUser()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                List<User> users = new List<User>();
                SqlDataReader sqlReader = null;
                User user = null;
                try
                {
                    CreateProcedureCommand("Users_get");

                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        user = new User();
                        user.ID = sqlReader.GetInt32(0);
                        user.Login = sqlReader.GetString(1);
                        user.Password = sqlReader.GetString(2);
                        user.Surname = sqlReader.GetString(3);
                        user.Forename = sqlReader.GetString(4);
                        user.Active = sqlReader.GetBoolean(5);
                        users.Add(user);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład ładowania danych użytkownika", ex);
                }
                return users;
            }
        }

        public User Login(string login, string password)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                User user = null;
                try
                {
                    CreateProcedureCommand("Users_Login");
                    AddParameter("@login", login);
                    AddParameter("@password", password);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        user = new User();
                        user.ID = sqlReader.GetInt32(0);
                        user.Login = sqlReader.GetString(1);
                        //      user.Password = sqlReader.GetString(2);
                        user.Surname = sqlReader.GetString(3);
                        user.Forename = sqlReader.GetString(4);
                        user.Active = sqlReader.GetBoolean(5);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład logowania", ex);
                }
                return user;
            }

        }

        public object Save(User user)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    if (user.ID == 0)
                    {
                        CreateProcedureCommand("Users_Insert");
                        AddOutputParameter("User_id");
                    }
                    else
                    {
                        CreateProcedureCommand("Users_Update");
                        AddParameter("user_id", user.ID);
                    }
                    AddParameter("login", user.Login);
                    AddParameter("password", user.Password);
                    AddParameter("forename", user.Forename);
                    AddParameter("surname", user.Surname);
                    AddParameter("active", user.Active);
                    AddParameter("Privileges", user.Privileges);
                    AddReturnParameter();
                    ExecuteProcedure();
                    int ret = GetReturnValue();
                    if (ret == 0)
                    {
                        if (user.ID == 0)
                            user.ID = GetParameterValue("User_id");
                    }
                    else if (ret == -1)
                        return "ERR";
                    else if (ret == -2)
                        return "ERR";
                    CommitTransaction();
                    return user;
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    return "ERR";
                }
            }
        }

        #endregion

        #region Product

        public Product GetProduct(int productID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                Product product = null;
                try
                {
                    CreateProcedureCommand("Products_Get");
                    if (productID != 0)
                        AddParameter("@id", productID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        product = new Product();
                        product.ID = sqlReader.GetInt32(0);
                        product.Code = sqlReader.GetString(1);
                        product.Name = sqlReader.GetString(2);
                        product.Group = new ProductGroup(sqlReader.GetInt32(3),
                                                            sqlReader.GetString(5),
                                                            true);
                        product.Active = sqlReader.GetBoolean(4);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania produktu", ex);
                }
                return product;

            }
        }

        public ProductList GetProducts(string codeOrName)
        {

            return GetProducts(codeOrName, 0, ExtendedBoolean.True);

        }

        public ProductList GetProducts(string codeOrName, int groupID, ExtendedBoolean active)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                ProductList productList = new ProductList();
                try
                {
                    CreateProcedureCommand("Products_Get");
                    AddParameter("@code_or_name", codeOrName);
                    if (groupID != 0)
                        AddParameter("@group_id", groupID);
                    if (active == ExtendedBoolean.True)
                        AddParameter("@active", 1);
                    else if (active == ExtendedBoolean.False)
                        AddParameter("@active", 0);

                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Product product = new Product();
                        product.ID = sqlReader.GetInt32(0);
                        product.Code = sqlReader.GetString(1);
                        product.Name = sqlReader.GetString(2);
                        product.Group = new ProductGroup(sqlReader.GetInt32(3),
                                                            sqlReader.GetString(5),
                                                            true);
                        product.Active = sqlReader.GetBoolean(4);
                        product.LastSaveDate = sqlReader.GetDateTime(6);
                        product.LastSaveUserID = sqlReader.GetInt32(7);
                        if (!sqlReader.IsDBNull(8))
                            product.Description = sqlReader.GetString(8);
                        productList.Add(product);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania produktów", ex);
                }
                return productList;
            }

        }

        public void SaveProduct(Product product)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    if (product.ID == 0)
                    {
                        CreateProcedureCommand("Products_Insert");
                        AddOutputParameter("product_id");
                    }
                    else
                    {
                        CreateProcedureCommand("Products_Update");
                        AddParameter("product_id", product.ID);
                    }
                    AddParameter("code", product.Code);
                    AddParameter("name", product.Name);
                    AddParameter("active", product.Active);
                    AddParameter("description", "");
                    AddParameter("pgroup_id", product.Group.ID);
                    AddParameter("last_save_user_id", product.LastSaveUserID);
                    AddReturnParameter();
                    ExecuteProcedure();
                    int ret = GetReturnValue();
                    if (ret == 0)
                    {
                        if (product.ID == 0)
                            product.ID = GetParameterValue("product_id");
                    }
                    else if (ret == -1)
                        throw new Exception("Istnieje juz produkt o podanym kodzie");
                    else if (ret == -2)
                        throw new Exception("Istnieje juz produkt o podanej nazwie");
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Blad podczas zapisu produktu", ex);
                }
            }
        }

        public void Delete(Product product)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("Products_Delete");
                    AddParameter("product_id", product.ID);
                    AddReturnParameter();
                    ExecuteProcedure();
                    int ret = GetReturnValue();
                    if (ret == 0)
                    {
                        if (product.ID == 0)
                            product.ID = GetParameterValue("product_id");
                    }
                    else if (ret == -1)
                        throw new Exception("Istnieje już produkt o podanym kodzie");
                    else if (ret == -2)
                        throw new Exception("Istnieje już produkt o podanej nazwie");
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu produktu", ex);
                }
            }
        }

        #endregion

        #region ProductGroup

        public ProductGroupList GetProductGroups()
        {

            return GetProductGroups(ExtendedBoolean.True);
        }

        public ProductGroupList GetProductGroups(ExtendedBoolean active)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                ProductGroupList groupList = new ProductGroupList();
                try
                {
                    CreateProcedureCommand("ProductGroups_Get");
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        ProductGroup pgroup = new ProductGroup(sqlReader.GetInt32(0),
                                                                sqlReader.GetString(1),
                                                                sqlReader.GetBoolean(2));
                        groupList.Add(pgroup);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład ładoawania grup produktów", ex);
                }
                return groupList;
            }
        }

        public void Save(ProductGroup productGroup)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    if (productGroup.ID == 0)
                    {
                        CreateProcedureCommand("ProductGroups_Insert");
                        AddOutputParameter("pgroup_id");
                    }
                    else
                    {
                        CreateProcedureCommand("ProductGroups_Update");
                        AddParameter("pgroup_id", productGroup.ID);
                    }
                    AddParameter("name", productGroup.Name);
                    AddParameter("active", productGroup.Active);
                    AddReturnParameter();
                    ExecuteProcedure();
                    int ret = GetReturnValue();
                    if (ret == 0)
                    {
                        if (productGroup.ID == 0)
                            productGroup.ID = GetParameterValue("pgroup_id");
                    }
                    else if (ret == -1)
                        throw new Exception("Istnieje już grupa produktów o podanej nazwie");
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu produktu", ex);
                }
            }
        }

        #endregion

        #region ProductInStock

        public StockTakingProductList GetProductInStock(int storeID, int pgroupID, int productID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StockTakingProductList productList = new StockTakingProductList();
                try
                {
                    CreateProcedureCommand("Products_GetProductInStock");
                    if (storeID != 0)
                        AddParameter("@store_id", storeID);
                    if (pgroupID != 0)
                        AddParameter("@pgroup_id", pgroupID);
                    if (productID != 0)
                        AddParameter("@product_id", productID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        StockTakingProduct product = new StockTakingProduct();
                        product.StockQty = sqlReader.GetInt32(0);
                        product.ProductID = sqlReader.GetInt32(1);
                        product.ProductCode = sqlReader.GetString(2);
                        product.ProductName = sqlReader.GetString(3);
                        //product.ProductGroupID = sqlReader.GetInt32(3);
                        //product.ProductGroupName = sqlReader.GetString(4);
                        //product.StoreID = sqlReader.GetInt32(5);
                        //product.StoreName = sqlReader.GetString(6);
                        productList.Add(product);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania produktów", ex);
                }
                return productList;
            }
        }

        #endregion


        #region Tag

        public Tag GetTag(string code)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                try
                {
                    CreateProcedureCommand("Tags_Get");
                    AddParameter("@code", code);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Tag tag = new Tag();
                        tag.ID = sqlReader.GetInt32(0);
                        tag.Code = sqlReader.GetString(1);
                        tag.StatusCode = sqlReader.GetString(2);
                        tag.ProductID = sqlReader.GetInt32(3);
                        tag.ProductCode = sqlReader.GetString(4);
                        tag.ProductName = sqlReader.GetString(5);
                        if (!sqlReader.IsDBNull(6))
                        {
                            tag.StoreID = sqlReader.GetInt32(6);
                            tag.StoreName = sqlReader.GetString(7);
                        }
                        if (!sqlReader.IsDBNull(8))
                        {
                            tag.CustomerID = sqlReader.GetInt32(8);
                            tag.CustomerName = sqlReader.GetString(9);
                        }
                        sqlReader.Close();
                        return tag;
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania taga", ex);
                }
                return null;
            }
        }

        public Tag GetTagNoDbConnection(string code)
        {
           
                SqlDataReader sqlReader = null;
                try
                {
                    CreateProcedureCommand("Tags_Get");
                    AddParameter("@code", code);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Tag tag = new Tag();
                        tag.ID = sqlReader.GetInt32(0);
                        tag.Code = sqlReader.GetString(1);
                        tag.StatusCode = sqlReader.GetString(2);
                        tag.ProductID = sqlReader.GetInt32(3);
                        tag.ProductCode = sqlReader.GetString(4);
                        tag.ProductName = sqlReader.GetString(5);
                        if (!sqlReader.IsDBNull(6))
                        {
                            tag.StoreID = sqlReader.GetInt32(6);
                            tag.StoreName = sqlReader.GetString(7);
                        }
                        if (!sqlReader.IsDBNull(8))
                        {
                            tag.CustomerID = sqlReader.GetInt32(8);
                            tag.CustomerName = sqlReader.GetString(9);
                        }
                        sqlReader.Close();
                        return tag;
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania taga", ex);
                }
                return null;
           
        }
        public Tag GetTag(int id)
        {


            SqlDataReader sqlReader = null;
            try
            {
                CreateProcedureCommand("Tags_Get");
                AddParameter("@id", id);
                sqlReader = ExecuteQuery();
                while (sqlReader.Read())
                {
                    Tag tag = new Tag();
                    tag.ID = sqlReader.GetInt32(0);
                    tag.Code = sqlReader.GetString(1);
                    tag.StatusCode = sqlReader.GetString(2);
                    tag.ProductID = sqlReader.GetInt32(3);
                    tag.ProductCode = sqlReader.GetString(4);
                    tag.ProductName = sqlReader.GetString(5);
                    if (!sqlReader.IsDBNull(6))
                    {
                        tag.StoreID = sqlReader.GetInt32(6);
                        tag.StoreName = sqlReader.GetString(7);
                    }
                    if (!sqlReader.IsDBNull(8))
                    {
                        tag.CustomerID = sqlReader.GetInt32(8);
                        tag.CustomerName = sqlReader.GetString(9);
                    }
                    sqlReader.Close();
                    return tag;
                }
                sqlReader.Close();
            }
            catch (Exception ex)
            {
                if (sqlReader != null)
                    sqlReader.Close();
                throw new DatabaseManagerException("Bład wyszukiwania taga", ex);
            }
            return null;

        }
        public TagList TagByProductId(int ProductId)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                TagList tags = new TagList();
                SqlDataReader sqlReader = null;
                try
                {
                    CreateProcedureCommand("Tags_GetByCode");
                    AddParameter("@ProductID", ProductId);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Tag tag = new Tag();
                        tag.ID = sqlReader.GetInt32(0);
                        tag.Code = sqlReader.GetString(1);
                        tag.ProductID = sqlReader.GetInt32(2);
                        tag.StatusCode = sqlReader.GetString(3);
                        tag.ProductCode = sqlReader.GetString(4);
                        tag.ProductName = sqlReader.GetString(5);
                        if (!sqlReader.IsDBNull(6))
                        {
                            tag.StoreID = sqlReader.GetInt32(6);
                            // tag.StoreName = sqlReader.GetString(7);
                        }
                        if (!sqlReader.IsDBNull(7))
                        {
                            tag.CustomerID = sqlReader.GetInt32(7);
                            //tag.CustomerName = sqlReader.GetString(9);
                        }

                        tags.Add(tag);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania taga", ex);
                }
                return tags;
            }
        }

        public TagList GetTags(int storeID, int pgroupID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                TagList tags = new TagList();
                SqlDataReader sqlReader = null;
                try
                {
                    CreateProcedureCommand("Tags_Get");
                    if (storeID > 0)
                        AddParameter("@store_id", storeID);
                    if (pgroupID > 0)
                        AddParameter("@pgroup_id", pgroupID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Tag tag = new Tag();
                        tag.ID = sqlReader.GetInt32(0);
                        tag.Code = sqlReader.GetString(1);
                        tag.StatusCode = sqlReader.GetString(2);
                        tag.ProductID = sqlReader.GetInt32(3);
                        tag.ProductCode = sqlReader.GetString(4);
                        tag.ProductName = sqlReader.GetString(5);
                        if (!sqlReader.IsDBNull(6))
                        {
                            tag.StoreID = sqlReader.GetInt32(6);
                            tag.StoreName = sqlReader.GetString(7);
                        }
                        if (!sqlReader.IsDBNull(8))
                        {
                            tag.CustomerID = sqlReader.GetInt32(8);
                            tag.CustomerName = sqlReader.GetString(9);
                        }
                        tags.Add(tag);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania tagów", ex);
                }
                return tags;
            }
        }

        #endregion

        #region Operation

        public void SaveTagRegAndInsert(Operation operation, List<Tag> tags, int productID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", "R");
                    AddParameter("user_id", operation.UserID);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");

                    foreach (Tag tag in tags)
                    {
                        int tagID = tag.ID;
                        if (tag.ID == 0)
                        {
                            CreateProcedureCommand("Tags_Insert");
                            AddParameter("code", FormatEPC(tag.Code));
                            AddParameter("product_id", productID);
                            if (tag.CustomerID > 0)
                                AddParameter("cust_id", tag.CustomerID);
                            AddOutputParameter("tag_id");
                            AddReturnParameter();
                            ExecuteProcedure();
                            int ret = GetReturnValue();
                            if (ret == 1)
                                throw new Exception(String.Format("Tag {0} jest już zarejestrowany", tag.Code));

                            tag.ID = GetParameterValue("tag_id");
                        }
                        else
                        {
                            CreateProcedureCommand("Tags_Update");
                            AddParameter("tag_id", tag.ID);
                            AddParameter("product_id", productID);
                            AddParameter("cust_id", tag.CustomerID);
                            AddParameter("status", "R");
                            ExecuteProcedure();
                        }
                        CreateProcedureCommand("OperationItems_Insert");
                        AddParameter("oh_id", operation.ID);
                        AddParameter("tag_id", tag.ID);
                        AddParameter("product_id", productID);
                        ExecuteProcedure();
                    }


                    operation.Type = OperationType.Input;
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    AddParameter("in_store_id", operation.InStoreID);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    foreach (Tag tag in tags)
                    {
                        CreateProcedureCommand("Tags_Update");
                        AddParameter("tag_id", tag.ID);
                        AddParameter("product_id", productID);
                        if (tag.CustomerID > 0)
                            AddParameter("cust_id", tag.CustomerID);
                        AddParameter("status", "I");
                        AddParameter("store_id", operation.InStoreID);
                        ExecuteProcedure();

                        CreateProcedureCommand("OperationItems_Insert");
                        AddParameter("oh_id", operation.ID);
                        AddParameter("tag_id", tag.ID);
                        AddParameter("product_id", productID);
                        ExecuteProcedure();
                    }

                    CommitTransaction();



                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }


            }

        }



        ///TUTAJ zatwierdzony blad://///
        public void SaveTagRegistration(Operation operation, List<Tag> tags, int productID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");
                    if (operation.Type == OperationType.Registration)
                    {
                        foreach (Tag tag in tags)
                        {
                            Tag t = GetTagNoDbConnection(FormatEPC(tag.Code));
                            if (t != null)
                                tag.ID = t.ID;

                            int tagID = tag.ID;
                            if (tag.ID == 0)
                            {
                                CreateProcedureCommand("Tags_Insert");
                                AddParameter("code", FormatEPC(tag.Code));
                                AddParameter("product_id", productID);
                                AddOutputParameter("tag_id");
                                AddReturnParameter();
                                ExecuteProcedure();
                                int ret = GetReturnValue();
                                if (ret == 1)
                                    throw new Exception(String.Format("Tag {0} jest już zarejestrowany", tag.Code));

                                tagID = GetParameterValue("tag_id");
                            }
                            else
                            {
                                CreateProcedureCommand("Tags_Update");
                                AddParameter("tag_id", tag.ID);
                                AddParameter("product_id", productID);
                                AddParameter("status", "R");
                                ExecuteProcedure();
                            }
                            CreateProcedureCommand("OperationItems_Insert");
                            AddParameter("oh_id", operation.ID);
                            AddParameter("tag_id", tagID);
                            AddParameter("product_id", productID);
                            ExecuteProcedure();
                        }
                    }
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }
            }
        }

        public void SaveTagInput(Operation operation, List<Tag> tags)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    if (operation.InStoreID > 0)
                        AddParameter("in_store_id", operation.InStoreID);
                    if (operation.CustomerID > 0)
                        AddParameter("cust_id", operation.CustomerID);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");
                    SaveTagInputList(operation, tags);
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }
            }
        }

        private void SaveTagInputList(Operation operation, List<Tag> tags)
        {

            foreach (Tag tag in tags)
            {
                Tag t;
                if (tag.ID == 0)
                    t = GetTag(FormatEPC(tag.Code));
                else
                    t = GetTag(tag.ID);
                if (t == null)
                    throw new DatabaseManagerException("Tag '" + tag.Code + "' nie jest zarejestrowany");
                //  if (t.Status == TagStatus.In)
                //     throw new DatabaseManagerException("Tag '" + tag.Code + "' nie może zostać przyjęty ponieważ jest na stanie magazynu '" + t.StoreName + "'.");
                //if (t.CustomerID != 0 && t.CustomerID != operation.CustomerID)
                //    throw new DatabaseManagerException("Tag '" + tag.Code + "' nie może zostać przyjęty ponieważ został wydany klientowi '" + t.CustomerName + "'.");


                CreateProcedureCommand("Tags_Update");
                AddParameter("tag_id", t.ID);
                AddParameter("product_id", t.ProductID);
                AddParameter("status", "I");
                AddParameter("store_id", operation.InStoreID);
                if (operation.CustomerID > 0)
                    AddParameter("cust_id", operation.CustomerID);
                ExecuteProcedure();

                CreateProcedureCommand("OperationItems_Insert");
                AddParameter("oh_id", operation.ID);
                AddParameter("tag_id", t.ID);
                AddParameter("product_id", t.ProductID);
                ExecuteProcedure();

            }
        }

        public void SaveTagOutput(Operation operation, List<Tag> tags)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    if (operation.OutStoreID > 0)
                        AddParameter("out_store_id", operation.OutStoreID);
                    else
                        AddParameter("out_store_id", null);
                    if (operation.CustomerID > 0)
                        AddParameter("cust_id", operation.CustomerID);
                    else
                        AddParameter("cust_id", null);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");
                    SaveTagOutputList(operation, tags);
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }
            }
        }

        private void SaveTagOutputList(Operation operation, List<Tag> tags)
        {

            foreach (Tag tag in tags)
            {
                Tag t;
                if (tag.ID == 0)
                    t = GetTag(FormatEPC(tag.Code));
                else
                    t = GetTag(tag.ID);
                if (t == null)
                    throw new DatabaseManagerException("Tag '" + tag.Code + "' nie jest zarejestrowany");
                // if (t.Status == TagStatus.Out)
                //   throw new DatabaseManagerException("Tag '" + tag.Code + "' Jest juz wydany");
                // if (t.StoreID != operation.OutStoreID)
                // {
                //     operation.OutStoreID = t.StoreID;
                // throw new DatabaseManagerException("Tag '" + tag.Code + "' nie może zostać wydany z wybranego magazynu, ponieważ nie ma go na jego stanie.");



                // }

                CreateProcedureCommand("Tags_Update");
                AddParameter("tag_id", tag.ID);
                AddParameter("product_id", tag.ProductID);
                AddParameter("status", "O");
                if (operation.OutStoreID > 0)
                    AddParameter("store_id", operation.OutStoreID);
                if (operation.CustomerID > 0)
                    AddParameter("cust_id", operation.CustomerID);

                ExecuteProcedure();

                CreateProcedureCommand("OperationItems_Insert");
                AddParameter("oh_id", operation.ID);
                AddParameter("tag_id", tag.ID);
                AddParameter("product_id", tag.ProductID);
                ExecuteProcedure();
            }

        }

        #endregion

        #region Customer

        public CustomerList GetCustomers(string codeOrName)
        {
            return GetCustomers(codeOrName, 0);
        }

        public CustomerList GetCustomers(string codeOrName, int groupID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                CustomerList customerList = new CustomerList();
                try
                {
                    CreateProcedureCommand("Customers_Get");
                    AddParameter("@code_or_name", codeOrName);
                    if (groupID > 0)
                        AddParameter("@cgroup_id", groupID);
                    AddParameter("@active", 1);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Customer cust = new Customer();
                        cust.ID = sqlReader.GetInt32(0);
                        cust.Code = sqlReader.GetString(1);
                        cust.Name = sqlReader.GetString(2);
                        cust.Active = sqlReader.GetBoolean(3);
                        cust.Group = new CustomerGroup(sqlReader.GetInt32(4),
                                                            sqlReader.GetString(5),
                                                            sqlReader.GetBoolean(6));
                        //if (!sqlReader.IsDBNull(5))
                        //{
                        //    cust.Store_ID = sqlReader.GetInt32(5);
                        //}
                        customerList.Add(cust);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania klientów", ex);
                }
                return customerList;
            }
        }

        #endregion

        #region CustomerGroup

        public CustomerGroupList GetCustomerGroups()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                CustomerGroupList groupList = new CustomerGroupList();
                try
                {
                    CreateProcedureCommand("CustomerGroups_Get");
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        CustomerGroup cgroup = new CustomerGroup(sqlReader.GetInt32(0),
                                                                sqlReader.GetString(1),
                                                                sqlReader.GetBoolean(2));
                        groupList.Add(cgroup);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład ładoawania grup klientów", ex);
                }
                return groupList;
            }
        }

        #endregion

        #region Store

        public StoreList GetStores()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StoreList storeList = new StoreList();
                try
                {
                    CreateProcedureCommand("Stores_Get");
                    AddParameter("@active", 1);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Store store = new Store();
                        store.ID = sqlReader.GetInt32(0);
                        store.Name = sqlReader.GetString(1);
                        store.Active = sqlReader.GetBoolean(2);
                        storeList.Add(store);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();

                    throw new DatabaseManagerException("Bład wyszukiwania magazynów", ex);
                    
                }
                return storeList;

            }
        }

        #endregion

        #region StockTaking

        public void Save(StockTaking stockTaking)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    int stocktagkingid = stockTaking.ID;
                    BeginTransaction();


                    CreateProcedureCommand("StockTakings_Insert");
                    AddParameter("snapshot_date", stockTaking.SnapshotDate);
                    AddParameter("user_id", stockTaking.UserID);
                    AddParameter("store_id", stockTaking.StoreID);
                    if (stockTaking.ProductGroupID > 0)
                        AddParameter("pgroup_id", stockTaking.ProductGroupID);
                    AddParameter("stock_taking_date", stockTaking.StockTakingDate);
                    AddParameter("status", stockTaking.StatusCode);
                    AddParameter("notes", stockTaking.Notes);
                    AddOutputParameter("stock_taking_id");
                    ExecuteProcedure();
                    stockTaking.ID = GetParameterValue("stock_taking_id");

                    foreach (StockTakingProduct stProduct in stockTaking.Products)
                    {
                        CreateProcedureCommand("StockTakingProducts_Insert");
                        AddParameter("stock_taking_id", stockTaking.ID);
                        AddParameter("product_id", stProduct.ProductID);
                        AddParameter("product_code", stProduct.ProductCode);
                        AddParameter("product_name", stProduct.ProductName);
                        AddParameter("snapshot_count", stProduct.StockQty);
                        AddParameter("tag_in_store_count", stProduct.CountedQty);
                        AddParameter("tag_out_store_count", stProduct.OutOfStoreQty);
                        AddParameter("tag_in_other_store_count", stProduct.OtherStoreQty);
                        AddParameter("tag_registered_count", stProduct.RegisteredQty);
                        AddParameter("correction_count", stProduct.CorrectionQty);
                        ExecuteProcedure();

                        foreach (StockTakingTag stTag in stProduct.Tags)
                        {
                            CreateProcedureCommand("StockTakingTags_Insert");
                            AddParameter("stock_taking_id", stockTaking.ID);
                            AddParameter("product_id", stProduct.ProductID);
                            AddParameter("tag_id", stTag.Tag.ID);
                            AddParameter("status", stTag.Tag.StatusCode);
                            if (stTag.Tag.StoreID > 0)
                                AddParameter("store_id", stTag.Tag.StoreID);
                            if (stTag.Tag.CustomerID > 0)
                                AddParameter("cust_id", stTag.Tag.CustomerID);
                            if (stTag.ReadDate > DateTime.MinValue)
                                AddParameter("read_date", stTag.ReadDate);
                            AddParameter("correction", stTag.CorrectionCode);
                            ExecuteProcedure();
                        }
                    }



                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu inwentaryzacji", ex);
                }
            }
        }

        public void StockTakingUpdateStatus(string status, int stocktaking_id)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("StockTakings_UpdateStatus");
                    AddParameter("@status", status);
                    AddParameter("@stock_taking_id", stocktaking_id);

                    ExecuteProcedure();
                    CommitTransaction();

                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zmiany statusu inwentaryzacji", ex);
                }
            }
        }

        public StockTakingList GetStockTakings(DateTime dateFrom, DateTime dateTo, string status, int storeID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StockTakingList stockTakingList = new StockTakingList();
                try
                {
                    CreateProcedureCommand("StockTakings_Get");
                    if (dateFrom != DateTime.MinValue)
                        AddParameter("@date_from", dateFrom);
                    if (dateTo != DateTime.MinValue)
                        AddParameter("@date_to", dateTo);
                    if (!String.IsNullOrEmpty(status))
                        AddParameter("@status", status);
                    if (storeID != 0)
                        AddParameter("@store_id", storeID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        StockTaking st = new StockTaking();
                        st.ID = sqlReader.GetInt32(0);
                        st.StockTakingDate = sqlReader.GetDateTime(1);
                        st.StatusCode = sqlReader.GetString(2);
                        st.StoreID = sqlReader.GetInt32(3);
                        st.StoreName = sqlReader.GetString(4);
                        st.SnapshotDate = sqlReader.GetDateTime(5);
                        st.SaveDate = sqlReader.GetDateTime(6);
                        if (!sqlReader.IsDBNull(7))
                        {
                            st.ProductGroupID = sqlReader.GetInt32(7);
                            st.ProductGroupName = sqlReader.GetString(8);
                        }
                        st.UserID = sqlReader.GetInt32(9);
                        if (!sqlReader.IsDBNull(10))
                            st.Notes = sqlReader.GetString(10);
                        stockTakingList.Add(st);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania inwentaryzacji", ex);
                }
                return stockTakingList;

            }
        }

        public StockTakingProductList GetStockTakingProducts(int stockTakingID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StockTakingProductList stockTakingProductList = new StockTakingProductList();
                try
                {
                    CreateProcedureCommand("StockTakingProducts_Get");
                    if (stockTakingID > 0)
                        AddParameter("@stock_taking_id", stockTakingID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        StockTakingProduct stp = new StockTakingProduct();
                        stp.ProductID = sqlReader.GetInt32(0);
                        stp.ProductCode = sqlReader.GetString(1);
                        stp.ProductName = sqlReader.GetString(2);
                        stp.StockQty = sqlReader.GetInt32(3);
                        stp.CountedQty = sqlReader.GetInt32(4);
                        stp.OutOfStoreQty = sqlReader.GetInt32(5);
                        stp.OtherStoreQty = sqlReader.GetInt32(6);
                        stp.RegisteredQty = sqlReader.GetInt32(7);
                        if (!sqlReader.IsDBNull(8))
                            stp.CorrectionQty = sqlReader.GetInt32(8);
                        stockTakingProductList.Add(stp);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania danych inwentaryzacji", ex);
                }
                return stockTakingProductList;

            }
        }

        public StockTakingTagList GetStockTakingTags(int stockTakingID)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StockTakingTagList stockTakingTagList = new StockTakingTagList();
                try
                {
                    CreateProcedureCommand("StockTakingTags_Get");
                    if (stockTakingID > 0)
                        AddParameter("@stock_taking_id", stockTakingID);
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        StockTakingTag stt = new StockTakingTag(new Tag());
                        stt.Tag.ProductID = sqlReader.GetInt32(0);
                        stt.Tag.ProductCode = sqlReader.GetString(1);
                        stt.Tag.ProductName = sqlReader.GetString(2);
                        stt.Tag.ID = sqlReader.GetInt32(3);
                        stt.Tag.Code = sqlReader.GetString(4);
                        stt.Tag.StatusCode = sqlReader.GetString(5);
                        if (!sqlReader.IsDBNull(6))
                        {
                            stt.ReadDate = sqlReader.GetDateTime(6);
                            stt.Counted = true;
                        }
                        if (!sqlReader.IsDBNull(7))
                        {
                            stt.Tag.StoreID = sqlReader.GetInt32(7);
                            stt.Tag.StoreName = sqlReader.GetString(8);
                        }
                        if (!sqlReader.IsDBNull(9))
                        {
                            stt.Tag.CustomerID = sqlReader.GetInt32(9);
                            stt.Tag.CustomerName = sqlReader.GetString(10);
                        }
                        stt.CorrectionCode = sqlReader.GetString(11);
                        stockTakingTagList.Add(stt);
                    }
                    sqlReader.Close();


                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania danych inwentaryzacji", ex);
                }
                return stockTakingTagList;
            }
        }

        public void Save(StockTaking stockTaking, Operation inputOperation, List<Tag> inputTags, Operation outputOperation, List<Tag> outputTags)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    if (inputOperation != null)
                        SaveTagInput(inputOperation, inputTags);
                    if (outputOperation != null)
                        SaveTagOutput(outputOperation, outputTags);
                    Save(stockTaking);
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu korekty inwentaryzacji", ex);
                }
            }
        }

        #endregion

        #region TagReading

        public void Save(TagReading reading, DateTime saveDate)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("Readings_Insert");
                    AddParameter("tag_code", reading.TagCode);
                    AddParameter("reading_date", reading.ReadDate);
                    AddParameter("ra_id", reading.AntennaID);
                    AddParameter("save_date", saveDate);
                    ExecuteProcedure();
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu odczytu", ex);
                }
            }
        }

        #endregion

        #region TagOnGateReading

        public void Save(TagOnGateReading reading, int gateID, DateTime saveDate)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("GateReadings_Insert");
                    AddParameter("gate_id", gateID);
                    AddParameter("tag_code", reading.TagCode);
                    AddParameter("first_detection_date", reading.FirstDetectionDate);
                    AddParameter("last_detection_date", reading.LastDetectionDate);
                    AddParameter("save_date", saveDate);
                    AddParameter("status", 0);
                    ExecuteProcedure();
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu odczytu", ex);
                }
            }
        }

        #endregion

        #region Plant

        public PlantList GetPlants()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                PlantList plantList = new PlantList();
                try
                {
                    CreateProcedureCommand("Plants_Get");
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Plant plant = new Plant();
                        plant.ID = sqlReader.GetInt32(0);
                        plant.Name = sqlReader.GetString(1);
                        plant.Code = sqlReader.GetString(2);
                        plantList.Add(plant);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania fabryk", ex);
                }
                return plantList;

            }
        }

        #endregion

        #region StorageLocation

        public StorageLocationList GetStorageLocations()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                StorageLocationList slList = new StorageLocationList();
                try
                {
                    CreateProcedureCommand("StorageLocations_Get");
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        StorageLocation sl = new StorageLocation();
                        sl.ID = sqlReader.GetInt32(0);
                        sl.Name = sqlReader.GetString(1);
                        sl.Code = sqlReader.GetString(2);
                        slList.Add(sl);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania miejsc składowania", ex);
                }
                return slList;

            }
        }

        #endregion

        #region Unit

        public UnitList GetUnits()
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                SqlDataReader sqlReader = null;
                UnitList unitList = new UnitList();
                try
                {
                    CreateProcedureCommand("Units_Get");
                    sqlReader = ExecuteQuery();
                    while (sqlReader.Read())
                    {
                        Unit unit = new Unit();
                        unit.ID = sqlReader.GetInt32(0);
                        unit.Name = sqlReader.GetString(1);
                        unit.Code = sqlReader.GetString(2);
                        unitList.Add(unit);
                    }
                    sqlReader.Close();
                }
                catch (Exception ex)
                {
                    if (sqlReader != null)
                        sqlReader.Close();
                    throw new DatabaseManagerException("Bład wyszukiwania jednostek miary", ex);
                }
                return unitList;

            }
        }

        #endregion

        #region InputOperation

        public void SaveInputOperation(InputOperation operation, List<Tag> tags)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    AddParameter("in_store_id", operation.InStoreID);
                    if (operation.CustomerID > 0)
                        AddParameter("cust_id", operation.CustomerID);
                    AddParameter("order_no", operation.OrderNo);
                    AddParameter("batch_no", operation.BatchNo);
                    AddParameter("plant_id", operation.Plant.ID);
                    AddParameter("sl_id", operation.StorageLocation.ID);
                    AddParameter("unit_id", operation.Unit.ID);
                    AddParameter("production_date", operation.ProductionDate);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");
                    SaveTagInputList(operation, tags);
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }
            }
        }

        #endregion

        #region OutputOperation

        public void SaveOutputOperation(OutputOperation operation, List<Tag> tags)
        {
            using (m_Connection = new SqlConnection(connectionString))
            {
                m_Connection.Open();
                try
                {
                    BeginTransaction();
                    CreateProcedureCommand("OperationHeaders_Insert");
                    AddParameter("type", operation.TypeCode);
                    AddParameter("user_id", operation.UserID);
                    AddParameter("out_store_id", operation.OutStoreID);
                    if (operation.CustomerID > 0)
                        AddParameter("cust_id", operation.CustomerID);
                    AddParameter("shipment_no", operation.ShipmentNo);
                    AddParameter("order_no", operation.OrderNo);
                    AddParameter("batch_no", operation.BatchNo);
                    AddParameter("plant_id", operation.Plant.ID);
                    AddParameter("sl_id", operation.StorageLocation.ID);
                    AddParameter("unit_id", operation.Unit.ID);
                    AddParameter("production_date", operation.ProductionDate);
                    AddOutputParameter("oh_id");
                    ExecuteProcedure();
                    operation.ID = GetParameterValue("oh_id");
                    SaveTagOutputList(operation, tags);
                    CommitTransaction();
                }
                catch (Exception ex)
                {
                    RollbackTransaction();
                    throw new DatabaseManagerException("Bład podczas zapisu operacji", ex);
                }
            }
        }

        #endregion
    }
}
