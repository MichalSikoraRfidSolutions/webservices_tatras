using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class CustomerGroup
    {
        public CustomerGroup()
        {
        }

        public CustomerGroup(int id, string name, bool active)
        {
            m_ID = id;
            m_Name = name;
            m_Active = active;
        }

        #region ID

        protected int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Name

        protected string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        #region Active

        protected bool m_Active;

        public bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        #endregion
    }

    public class CustomerGroupList : List<CustomerGroup>
    {
    }
}
