﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class WSOutput
    {

        public Operation Operation { get; set; }

        public List<Tag> Tags { get; set; }

        public WSOutput()
        {
            Tags = new List<Tag>();
        }

    }
}
