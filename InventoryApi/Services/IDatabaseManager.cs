using System;
using System.Collections.Generic;
using System.Text;
using RFIDObjects.MetaModel;
namespace InventoryApi.Services
{ 
    public interface IDatabaseManager
    {
        void OpenConnection(string connectionString);
        void CloseConnection();

        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

        void ExecuteProcedure(string procedureName);

        List<User> LoadUser();
        User Login(string login, string password);
        object Save(User user);

        Product GetProduct(int productID);
        ProductList GetProducts(string codeOrName);
        ProductList GetProducts(string codeOrName, int groupID, ExtendedBoolean active);
        void SaveProduct(Product product);
        void Delete(Product product);

        ProductGroupList GetProductGroups();
        ProductGroupList GetProductGroups(ExtendedBoolean active);
        void Save(ProductGroup productGroup);

        StockTakingProductList GetProductInStock(int storeID, int pgroupID, int productID);

       

        Tag GetTag(string code);
        TagList GetTags(int storeID, int pgroupID);
        TagList TagByProductId(int ProductId);
        void SaveTagRegistration(Operation operation, List<Tag> tags, int productID);
        void SaveTagInput(Operation operation, List<Tag> tags);
        void SaveTagOutput(Operation operation, List<Tag> tags);

        CustomerList GetCustomers(string codeOrName);
        CustomerList GetCustomers(string codeOrName, int groupID);
        //Customer GetCustomer(int id);

        CustomerGroupList GetCustomerGroups();

        StoreList GetStores();
        //Store GetStore(int id);

        void Save(StockTaking stockTaking);
        void Save(StockTaking stockTaking, Operation inputOperation, List<Tag> inputTags, Operation outputOperation, List<Tag> outputTags);
        StockTakingList GetStockTakings(DateTime dateFrom, DateTime dateTo, string status, int storeID);
        StockTakingProductList GetStockTakingProducts(int stockTakingID);
        StockTakingTagList GetStockTakingTags(int stockTakingID);
        void StockTakingUpdateStatus(string status, int stocktaking_id);


        void Save(TagReading reading, DateTime saveDate);

        void Save(TagOnGateReading reading, int gateID, DateTime saveDate);

        PlantList GetPlants();
        StorageLocationList GetStorageLocations();
        UnitList GetUnits();
        void SaveInputOperation(InputOperation operation, List<Tag> tags);
        void SaveOutputOperation(OutputOperation operation, List<Tag> tags);
        void SaveTagRegAndInsert(Operation operation, List<Tag> tags, int productID);
    }

    public class DatabaseManagerException : Exception
    {
        public DatabaseManagerException(string msg)
            : base(msg)
        {
        }

        public DatabaseManagerException(string msg, Exception innerEx)
            : base(String.Format("{0}  {1}", msg, innerEx.Message), innerEx)
        {
        }
    }

    public enum ExtendedBoolean
    {
        Any,
        True,
        False
    }
}
