using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class Plant
    {
        #region ID

        protected int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Name

        protected string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        #region Code

        protected string m_Code;

        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }

        #endregion
    }

    public class PlantList : List<Plant>
    {
    }

}
