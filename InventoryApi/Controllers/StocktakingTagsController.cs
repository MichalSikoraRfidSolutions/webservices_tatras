﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocktakingTagsController : ControllerBase
    {
        private readonly IDatabaseManager database;

        public StocktakingTagsController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }
        // GET: api/StocktakingTags
        [HttpGet]
        public IActionResult Get(int stockTakingID)
        {
            try
            {
                var st = database.GetStockTakingTags(stockTakingID);
                return Ok(st);
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message); }
        }

     
    }
}
