﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryApi.Controllers
{
 
        [Route("api/[controller]")]
        [ApiController]
        public class ProductGroupsController : ControllerBase
        {
            private readonly IDatabaseManager database;
            public ProductGroupsController(IDatabaseManager idatabase)
            {
                database = idatabase;
            }
            // GET: api/Products
            [HttpGet]
            public IActionResult Get()
            {
            try
            {
                var products = database.GetProductGroups();
                return Ok(products);
            }
            catch(Exception ex)

            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message);
            }

        }
           


        }

}

 
 
