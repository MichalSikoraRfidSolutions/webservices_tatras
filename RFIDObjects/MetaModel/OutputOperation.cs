using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace RFIDObjects.MetaModel
{
    public class OutputOperation : Operation
    {
        public OutputOperation()
        {
            Type = OperationType.Output;
        }

        #region ShipmentNo

        private string m_ShipmentNo;
        public string ShipmentNo
        {
            get { return m_ShipmentNo; }
            set { m_ShipmentNo = value; }
        }

        #endregion

        #region OrderNo

        private string m_OrderNo;
        public string OrderNo
        {
            get { return m_OrderNo; }
            set { m_OrderNo = value; }
        }

        #endregion

        #region BatchNo

        private string m_BatchNo;
        public string BatchNo
        {
            get { return m_BatchNo; }
            set { m_BatchNo = value; }
        }

        #endregion

        #region Plant

        private Plant m_Plant;
        public Plant Plant
        {
            get { return m_Plant; }
            set { m_Plant = value; }
        }

        #endregion

        #region StorageLocation

        private StorageLocation m_StorageLocation;
        public StorageLocation StorageLocation
        {
            get { return m_StorageLocation; }
            set { m_StorageLocation = value; }
        }

        #endregion

        #region Unit

        private Unit m_Unit;
        public Unit Unit
        {
            get { return m_Unit; }
            set { m_Unit = value; }
        }

        #endregion

        #region ProductionDate

        private DateTime m_ProductionDate;
        public DateTime ProductionDate
        {
            get { return m_ProductionDate; }
            set { m_ProductionDate = value; }
        }

        #endregion

        #region SaveXML

        public void SaveXML(string folder, string filename)
        {
            if (String.IsNullOrEmpty(folder))
                return;

            string name = String.Format("{0}\\{1}{2}.xml", folder, filename, ID);
            XmlTextWriter xml = new XmlTextWriter(name, Encoding.GetEncoding("ISO-8859-1"));
            xml.Formatting = Formatting.Indented;
            xml.WriteStartDocument();
            xml.WriteStartElement("DELVRY03");
            xml.WriteStartElement("IDOC");
            xml.WriteAttributeString("BEGIN", "1");
            xml.WriteStartElement("EDI_DC40");
            xml.WriteAttributeString("SEGMENT", "1");
            xml.WriteElementString("TABNAM", "EDI_DC40");
            xml.WriteElementString("DOCREL", "620");
            xml.WriteElementString("IDOCTYP", "DELVRY03");
            xml.WriteElementString("MESTYP", "SHPCON");
            xml.WriteElementString("SNDPOR", "IDOCXML");
            xml.WriteElementString("SNDPRT", "LS");
            xml.WriteElementString("SNDPRN", "SAPBC");
            xml.WriteElementString("RCVPOR", "SAPPRD");
            xml.WriteElementString("RCVPRT", "LS");
            xml.WriteElementString("RCVPRN", "SAPSEND500");
            xml.WriteStartElement("E1EDL20");
            xml.WriteAttributeString("SEGMENT", "1");
            xml.WriteElementString("VBELN", ShipmentNo);
            xml.WriteStartElement("E1EDL18");
            xml.WriteAttributeString("SEGMENT", "1");
            xml.WriteElementString("QUALF", "PGI");
            xml.WriteEndElement();
            xml.WriteStartElement("E1EDL24");
            xml.WriteAttributeString("SEGMENT", "1");
            xml.WriteElementString("POSNR", "000010");
            xml.WriteElementString("MATNR", "000000000000014516");
            xml.WriteElementString("LFIMG", "3.000");
            xml.WriteEndElement();
            xml.WriteEndElement();
            xml.WriteEndElement();
            xml.WriteEndElement();
            xml.WriteEndElement();
            xml.WriteEndDocument();
            xml.Close();
        }

        public void SaveCSV(string folder, List<Tag> tags)
        {
            if (String.IsNullOrEmpty(folder))
                return;

            string name = String.Format("{0}\\OUT.txt", folder);
            StreamWriter file = new StreamWriter(name, true, Encoding.GetEncoding("ISO-8859-1"));
            if (file.BaseStream.Length == 0)
            {
                file.Write("PSTNG_DATE");
                file.Write("\t");
                file.Write("DOC_DATE");
                file.Write("\t");
                file.Write("REF_DOC_NO");
                file.Write("\t");
                file.Write("GM_CODE");
                file.Write("\t");
                file.Write("MATERIAL");
                file.Write("\t");
                file.Write("PLANT");
                file.Write("\t");
                file.Write("STGE_LOC");
                file.Write("\t");
                file.Write("BATCH");
                file.Write("\t");
                file.Write("MOVE_TYPE");
                file.Write("\t");
                file.Write("CUSTOMER");
                file.Write("\t");
                file.Write("ENTRY_QNT");
                file.Write("\t");
                file.Write("ENTRY_UOM");
                file.Write("\t");
                file.Write("DEL_NUMBER");
                file.Write("\t");
                file.Write("PO_ITEM");
                file.Write("\t");
                file.Write("MVT_IND");
                file.Write("\t");
                file.Write("PROD_DATE");
                file.WriteLine();
            }
            string date = ProductionDate.ToString("ddMMyyyy");
            file.Write(date);
            file.Write("\t");
            file.Write(date);
            file.Write("\t");
            file.Write(OrderNo);
            file.Write("\t");
            file.Write("01");
            file.Write("\t");
            file.Write(tags[0].ProductCode);
            file.Write("\t");
            file.Write(Plant.Code);
            file.Write("\t");
            file.Write(StorageLocation.Code);
            file.Write("\t");
            file.Write(BatchNo);
            file.Write("\t");
            file.Write("101");
            file.Write("\t");
            file.Write(CustomerID.ToString());
            file.Write("\t");
            file.Write(tags.Count.ToString());
            file.Write("\t");
            file.Write("01");
            file.Write("\t");
            file.Write(OrderNo);
            file.Write("\t");
            file.Write("B");
            file.Write("\t");
            file.Write("\t");
            file.Write(date);
            file.WriteLine();
            file.Close();
        }

        #endregion
    }
}
