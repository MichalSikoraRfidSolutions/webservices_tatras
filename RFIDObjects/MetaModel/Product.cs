using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class Product
    {
        #region ID

        protected int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Name

        protected string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        #region Active

        protected bool m_Active;

        public bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        #endregion

        #region Group

        protected ProductGroup m_Group;

        public ProductGroup Group
        {
            get { return m_Group; }
            set { m_Group = value; }
        }

        #endregion

        #region Code

        protected string m_Code;

        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }

        #endregion

        #region GroupName


        public string GroupName
        {
            get
            {
                if (m_Group != null)
                    return m_Group.Name;
                else
                    return null;
            }
        }

        #endregion

        #region Description

        protected string m_Description;

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        #endregion

        #region LastSaveDate

        protected DateTime m_LastSaveDate;

        public DateTime LastSaveDate
        {
            get { return m_LastSaveDate; }
            set { m_LastSaveDate = value; }
        }

        #endregion

        #region LastSaveUserID

        protected int m_LastSaveUserID;

        public int LastSaveUserID
        {
            get { return m_LastSaveUserID; }
            set { m_LastSaveUserID = value; }
        }

        #endregion
    }

    public class ProductList : List<Product>
    {
    }
}
