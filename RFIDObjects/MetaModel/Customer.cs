using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
    public class Customer
    {
        #region ID

        protected int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        #endregion

        #region Name

        protected string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        #region Active

        protected bool m_Active;

        public bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }

        #endregion

        #region Group

        protected CustomerGroup m_Group;

        public CustomerGroup Group
        {
            get { return m_Group; }
            set { m_Group = value; }
        }

        #endregion

        #region GroupName

        public string GroupName
        {
            get
            {
                if (m_Group != null)
                    return m_Group.Name;
                else
                    return null;
            }
        }

        #endregion

        #region Code

        protected string m_Code;

        public string Code
        {
            get { return m_Code; }
            set { m_Code = value; }
        }

        protected int m_Store_ID;

        public int Store_ID
        {
            get { return m_Store_ID; }
            set { m_Store_ID = value; }
        }

        #endregion
    }

    public class CustomerList : List<Customer>
    {
    }

}
