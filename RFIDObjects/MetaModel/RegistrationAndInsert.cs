﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDObjects.MetaModel
{
   public  class RegistrationAndInsert
    {
        public List<Tag> TagList { set; get; }
        public int Store_id { set; get; }
        public int User_id { set; get; }
        public int Product_id { set; get; }

    }
}
