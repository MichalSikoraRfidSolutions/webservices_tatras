﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RFIDObjects.MetaModel;
namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocktakingProductsController : ControllerBase
    {
        private readonly IDatabaseManager database;

        public StocktakingProductsController(IDatabaseManager idatabase)
        {
            database = idatabase;
        }
        // GET: api/StocktakingProducts
        [HttpGet]
        public IActionResult Get(int stockTakingID)
        {
            try
            {
                var sp = database.GetStockTakingProducts(stockTakingID);
                return Ok(sp);
            }
            catch(Exception ex)
            {
                Console.WriteLine(Environment.NewLine + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + "!!!!!!!!!!!!!!!!!!!!!!!!!!! ERROR" + Environment.NewLine + Environment.NewLine);
                return BadRequest(ex.Message); }
        }
 
    }
}
